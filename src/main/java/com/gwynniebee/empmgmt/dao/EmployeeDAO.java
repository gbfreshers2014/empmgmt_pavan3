/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;
import com.gwynniebee.empmgmt.mapper.EmployeeMapper;
import com.gwynniebee.empmgmt.pojos.Employee;

/**
 * @author pavan
 */
@RegisterMapper(EmployeeMapper.class)
public interface EmployeeDAO extends Transactional<EmployeeDAO> {

    /**
     * @param empId employee id
     * @param emailId employee email id
     * @param fName employee name
     * @param phone employee phone
     * @param status employee status
     * @return id of the inserted row
     */
    @GetGeneratedKeys
    @SqlUpdate("insert into Employee (empId,emailId,fName,phone,status) values(:empId,:emailId,:fName,:phone, :status)")
    int addEmployee(@Bind("empId") int empId, @Bind("emailId") String emailId, @Bind("fName") String fName, @Bind("phone") int phone,
            @Bind("status") int status);

    /**
     * @param empId to be set
     * @return updated record's id
     */
    @SqlUpdate("update Employee set status=0 where empId=:empId")
    int deleteEmployee(@Bind("empId") int empId);

    /**
     * @return List of Employees
     */
    @SqlQuery("select empId, emailId, fName, phone, status from Employee")
    List<Employee> getEmployeesDetails();

    /**
     * @param empId of Employee
     * @return name of Employee
     */
    @SqlQuery("select fName from Employee where empId:=empId")
    String getEmployeeNameById(@Bind("empId") int empId);

    /**
     * @return count of the Employees
     */
    @SqlQuery("select count(*) from Employee")
    int getCountOfEmployees();

    /**
     * closes the underlying connection.
     */
    void close();
}
