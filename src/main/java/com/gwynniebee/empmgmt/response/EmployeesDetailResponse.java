/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.empmgmt.response;

import java.util.List;

import com.gwynniebee.empmgmt.pojos.Employee;
import com.gwynniebee.rest.common.response.AbstractResponse;

/**
 * @author pavan
 */
public class EmployeesDetailResponse extends AbstractResponse {

   private List<Employee> ees;

    /**
     * @return list of employees
     */
    public List<Employee> getEes() {
        return ees;
    }

    /**
     * @param ees sets employees
     */
    public void setEes(List<Employee> ees) {
        this.ees = ees;
    }

}
