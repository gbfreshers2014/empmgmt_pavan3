package com.gwynniebee.empmgmt.test.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.empmgmt.dao.EmployeeDAO;
import com.gwynniebee.empmgmt.test.helper.LiquibaseOperations;
import com.gwynniebee.empmgmt.test.helper.TestDAOBase;

public class TestEmployeeDAO extends TestDAOBase {

    public static final Logger LOG = LoggerFactory.getLogger(TestEmployeeDAO.class);

    @Test
    public void testEmployeeDAO() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();
        DBI dbi = LiquibaseOperations.getDBI();
        EmployeeDAO edao = dbi.open(EmployeeDAO.class);
        try {
            int empId = 106629;
            String emailId = "vishnu@gwynniebee.com";
            String fName = "Vishnu";
            int phone = 288297;
            int status = 1;
            edao.begin();
            edao.addEmployee(empId, emailId, fName, phone, status);
            edao.commit();
            String eName1 = edao.getEmployeeNameById(106629);
            int count = edao.getCountOfEmployees();

            assertEquals("Vishnu", eName1);
            assertEquals(1, count);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            edao.close();
        }
    }
}
