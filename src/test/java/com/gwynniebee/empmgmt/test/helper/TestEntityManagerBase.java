package com.gwynniebee.empmgmt.test.helper;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.gwynniebee.empmgmt.test.helper.LiquibaseOperations;
import com.gwynniebee.test.helper.j2ee.J2eeJndiHelper;

/**
 * @author pavan
 *
 */
public class TestEntityManagerBase {
    private static final Logger LOG = LoggerFactory.getLogger(TestEntityManagerBase.class);

    /**
     * @throws Exception
     * sets all required things before test case starts execution
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOG.info("Executing BeforeClass: Creating databases and apply schema through Liquibase");
        LiquibaseOperations.completeLiquibaseReset();

       // J2eeJndiHelper.reset();
       // DataSource ds = J2eeJndiHelper.getDataSource("configuration/local/empmgmt.properties", "gwynniebee_users");
       // J2eeJndiHelper.bind(ConstantsForEmpMgmt.DATABASE_SOURCE_NAME, ds);

        // GBUsersApplication.setDbi(LiquibaseOperations.getDBI());
    //    DBI dbi = new DBI(ds);
      //  dbi.registerArgumentFactory(new DateTimeAF());
      //  EmpMgmtApplication.setDbi(dbi);
    }

    /**
     * @throws Exception
     * cleans up everything after test cases are done
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LiquibaseOperations.dropDatabase();
        LOG.info("Executing AfterClass: Dropping databases");
        J2eeJndiHelper.destroy();
    }

    /**
     * @throws Exception
     * executed for every test case
     */
    @Before
    public void setUp() throws Exception {
        LOG.debug("Setup test: Cleaning all tables");
        LiquibaseOperations.cleanTables();
    }

}
