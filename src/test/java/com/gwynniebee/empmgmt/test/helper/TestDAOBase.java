package com.gwynniebee.empmgmt.test.helper;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for DAO Test.
 * 
 * @author Pavan
 */
public class TestDAOBase {
	public static final Logger LOG = LoggerFactory.getLogger(TestDAOBase.class);

	/**
	 * @throws Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	    LiquibaseOperations.createDatabase();
	    LiquibaseOperations.createSchemaThroughChangeSet();
	    LOG.info("Executing BeforeClass: creating databases");
	}

	/**
	 * @throws Exception; tear down after class
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LiquibaseOperations.dropDatabase();
		LOG.info("Executing AfterClass: Dropping databases");
	}

	/**
	 * @throws Exception
	 * setup before test 
	 */
	@Before
	public void setUp() throws Exception {
		LOG.debug("Setup test: Cleaning all tables");
		LiquibaseOperations.cleanTables();
	}
}
